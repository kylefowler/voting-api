package com.uptake.vote.endpoint.route

import com.uptake.vote.endpoint.HasUser
import com.uptake.vote.endpoint.Params
import org.jetbrains.ktor.locations.location

class TokenMixin(val token: String?) : HasUser {
    override fun getUserToken(): String? {
        return token
    }
}

@location("/")
class Index : Params

@location("/poll/{id}")
data class PollDetail(
    val id: String,
    val token: String? = null
) : Params, HasUser by TokenMixin(token)

@location("/poll/{pollId}/vote")
data class PollAddVote(
    val pollId: String,
    val optionId: String? = null,
    val upvote: Boolean? = false,
    val downvote: Boolean? = false,
    val token: String? = null
) : Params, HasUser by TokenMixin(token)

@location("/poll/{pollId}/votes")
data class PollVotesList(
    val pollId: String,
    val choiceId: String,
    val token: String? = null
) : Params, HasUser by TokenMixin(token)

@location("/poll/{id}/addChoice")
data class PollAddChoice(
    val id: String? = null,
    val categoryId: String? = null,
    val name: String? = null,
    val description: String? = null,
    val thumbnail: String? = null
) : Params

@location("/poll/{id}/addCategory")
data class PollAddCategory(
    val id: String? = null,
    val name: String? = null
) : Params

@location("/polls")
class Polls : Params

@location("/polls/import")
class PollImport : Params

@location("/users/add")
data class UserAdd(
    val userId: String? = null,
    val name: String? = null,
    val googleToken: String? = null,
    val email:String? = null,
    val photo: String? = null
) : Params
