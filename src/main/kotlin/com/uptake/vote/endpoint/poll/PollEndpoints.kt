package com.uptake.vote.endpoint.poll

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.mongodb.client.model.Filters
import com.mongodb.client.model.UpdateOptions
import com.uptake.vote.data.polls.model.Poll
import com.uptake.vote.data.polls.model.PollCategory
import com.uptake.vote.data.polls.model.PollOption
import com.uptake.vote.data.polls.model.PollUser
import com.uptake.vote.data.polls.model.PollVote
import com.uptake.vote.data.services.MongoService
import com.uptake.vote.data.services.getCollection
import com.uptake.vote.endpoint.Endpoint
import com.uptake.vote.endpoint.HasUser
import com.uptake.vote.endpoint.Params
import com.uptake.vote.endpoint.get
import com.uptake.vote.endpoint.post
import com.uptake.vote.endpoint.route.PollAddCategory
import com.uptake.vote.endpoint.route.PollAddChoice
import com.uptake.vote.endpoint.route.PollAddVote
import com.uptake.vote.endpoint.route.PollDetail
import com.uptake.vote.endpoint.route.PollImport
import com.uptake.vote.endpoint.route.PollVotesList
import com.uptake.vote.endpoint.route.Polls
import com.uptake.vote.model.ListResponse
import com.uptake.vote.model.PollDetailResponse
import com.uptake.vote.model.PollSummary
import com.uptake.vote.model.RpcData
import com.uptake.vote.model.VoteResponse
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import org.bson.types.ObjectId
import org.jetbrains.ktor.application.ApplicationRequest
import org.jetbrains.ktor.application.receive
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpMethod
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.accept
import org.litote.kmongo.findOne
import org.litote.kmongo.toList
import org.litote.kmongo.updateOne
import java.io.InputStream
import java.io.InputStreamReader

fun Route.polls(endpoint: PollEndpoints) {
    get<Polls>(endpoint)
    get<PollDetail>(endpoint)
    get<PollVotesList>(endpoint)
    accept(ContentType.Application.FormUrlEncoded) {
        post<PollAddChoice>(endpoint)
        post<PollAddVote>(endpoint)
        post<PollAddCategory>(endpoint)
    }
    accept(ContentType.Application.Json) {
        post<PollImport>(endpoint)
    }
}

class PollEndpoints(val mongo: MongoService) : Endpoint {
    suspend override fun handleF(method: HttpMethod, request: ApplicationRequest, params: Params): RpcData {
        val user: PollUser? = if (params is HasUser) {
            if (params.getUserToken() == null) {
                throw IllegalStateException("Missing token")
            }
            mongo.getUser(params.getUserToken()!!)
        } else {
            null
        }
        return when (params) {
            is PollDetail -> pollDetail(ObjectId(params.id), user)
            is Polls -> polls()
            is PollAddVote -> addVote(params, user)
            is PollAddCategory -> addCategory(params)
            is PollImport -> importPoll(request.receive<InputStream>())
            is PollVotesList -> pollVotes(params)
            else -> throw Exception("no matching endpoint handler")
        }
    }

    private suspend fun pollVotes(params: PollVotesList): RpcData {
        val votes = async(CommonPool) {
            mongo.getCollection<PollVote>().find(Filters.and(Filters.eq("pollOptionId", params.choiceId))).toList()
        }
        return ListResponse(votes.await())
    }

    private suspend fun importPoll(body: InputStream) : RpcData {
        val poll = Gson().fromJson<Poll>(JsonReader(InputStreamReader(body)), Poll::class.java)
        val collection = mongo.getCollection<Poll>()

        collection.insertOne(poll)

        if (poll._id == null) {
            throw IllegalStateException("Poll Creation failed")
        }

        val categoryCollections = mongo.getCollection<PollCategory>()
        val optionCollection = mongo.getCollection<PollOption>()
        val categoriesToAdd = poll.categories?.map { it.copy(pollId = poll._id) }

        categoriesToAdd?.let {
            categoryCollections.insertMany(categoriesToAdd)
        }

        categoriesToAdd?.map { category ->
            async(CommonPool) {
                val mappedItems = category.options?.map {
                    it.copy(pollId = poll._id,categoryId = category._id)
                }
                optionCollection.insertMany(mappedItems)
            }
        }?.map { it.await() }

        return pollDetail(poll._id, null)
    }

    private suspend fun pollDetail(id: ObjectId, user: PollUser?) : RpcData {
        val poll = async(CommonPool) {
            mongo.getCollection<Poll>().findOne(Filters.eq("_id", id)) ?: throw Exception("Couldnt find poll: $id")
        }
        val options = async(CommonPool) {
            mongo.getCollection<PollOption>().find(Filters.eq("pollId", id))
        }
        val categories = async(CommonPool) {
            mongo.getCollection<PollCategory>().find(Filters.eq("pollId", id))
        }
        val votes = async(CommonPool) {
            mongo.getCollection<PollVote>().find(Filters.and(Filters.eq("pollId", id), Filters.eq("userId", user?._id)))
        }

        val categoryOptionMap = options.await().groupBy { it.categoryId }
        val voteOptionMap = votes.await().groupBy { it.pollOptionId }

        val completePoll = poll.await().copy(categories = categories.await().map {
                it.copy(options = categoryOptionMap.get(it._id)?.map {
                    it.copy(
                            hasDownvoted = voteOptionMap.get(it._id)?.getOrNull(0)?.downvote,
                            hasUpvoted = voteOptionMap.get(it._id)?.getOrNull(0)?.upvote
                    )
                })
            }.toList()
        )
        return PollDetailResponse(getVoteSummary(completePoll, user), completePoll)
    }

    private suspend fun polls() : RpcData {
        val collection = mongo.getCollection<Poll>("polls")
        val polls = collection.find().toList()
        return ListResponse(polls.map { it.copy(categories = null) })
    }

    private suspend fun addCategory(params: PollAddCategory) : RpcData {
        val collection = mongo.getCollection<PollCategory>()

        val pollId = ObjectId(params.id)
        val pollCategory = PollCategory(
                name = params.name ?: "",
                pollId = ObjectId(params.id)
        )
        collection.insertOne(pollCategory)
        return pollCategory
    }

    private suspend fun addVote(params: PollAddVote, user: PollUser?) : RpcData {
        val poll = async(CommonPool) {
            mongo.getCollection<Poll>().findOne(Filters.eq("_id", ObjectId(params.pollId))) ?: throw Exception("Couldnt find poll: ${params.pollId}")
        }
        val newVote = PollVote(
                pollOptionId = ObjectId(params.optionId),
                upvote = params.upvote ?: false,
                downvote = params.downvote ?: false,
                pollId = ObjectId(params.pollId),
                userId = user?._id
        )
        val option = mongo.getCollection<PollOption>().findOne(
                Filters.and(
                        Filters.eq("_id", ObjectId(params.optionId)),
                        Filters.eq("pollId", ObjectId(params.pollId))
                )
        ) ?: throw Exception("Option not found")

        val oldVote = mongo.getCollection<PollVote>().findOne(
                Filters.and(Filters.eq("userId", user?._id), Filters.eq("pollOptionId", newVote.pollOptionId))
        )

        val (upvoteAdjustment, downvoteAdjustment) = if (oldVote == null) {
            if (newVote.upvote) {
                Pair(1, 0)
            } else {
                Pair(0, 1)
            }
        } else {
            when {
                oldVote.upvote && newVote.downvote -> Pair(-1, 1)
                oldVote.downvote && newVote.upvote -> Pair(1, -1)
                else -> Pair(0,0)
            }
        }

        if (oldVote == null) {
            mongo.getCollection<PollVote>().insertOne(newVote)
        } else {
            mongo.getCollection<PollVote>().updateOne(newVote.copy(_id = oldVote?._id), UpdateOptions().upsert(true))
        }

        val updatedChoice = option.copy(
                upvoteCount = option.upvoteCount + upvoteAdjustment,
                downvoteCount = option.downvoteCount + downvoteAdjustment
        )
        mongo.getCollection<PollOption>().updateOne(updatedChoice)
        return VoteResponse(
                getVoteSummary(poll.await(), user),
                updatedChoice.copy(hasDownvoted = newVote.downvote, hasUpvoted = newVote.upvote)
        )
    }

    private suspend fun getVoteSummary(poll: Poll, user: PollUser?) : PollSummary {
        if (user == null) {
            return PollSummary()
        }
        val votes = async(CommonPool) {
            mongo.getCollection<PollVote>().find(Filters.and(Filters.eq("pollId", poll._id), Filters.eq("userId", user?._id)))
        }
        return PollSummary(poll.votesPerUser - votes.await().sumBy { if(it.downvote || it.upvote) 1 else 0 })
    }
}
