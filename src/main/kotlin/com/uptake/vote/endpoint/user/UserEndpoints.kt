package com.uptake.vote.endpoint.user

import com.mongodb.client.model.Filters
import com.uptake.vote.data.polls.model.PollUser
import com.uptake.vote.data.services.MongoService
import com.uptake.vote.data.services.getCollection
import com.uptake.vote.endpoint.Endpoint
import com.uptake.vote.endpoint.Params
import com.uptake.vote.endpoint.post
import com.uptake.vote.endpoint.route.UserAdd
import com.uptake.vote.model.RpcData
import org.jetbrains.ktor.application.ApplicationRequest
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpMethod
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.requestContentType
import org.litote.kmongo.findOne

/**
 * Created by kfowler on 5/4/17.
 */

fun Route.users(endpoint: UserEndpoints) {
    requestContentType(ContentType.Application.FormUrlEncoded) {
        post<UserAdd>(endpoint)
    }
}

class UserEndpoints(val mongo: MongoService) : Endpoint {
    suspend override fun handleF(method: HttpMethod, request: ApplicationRequest, params: Params): RpcData {
        return when (params) {
            is UserAdd -> addUser(params)
            else -> throw Exception("no matching endpoint handler")
        }
    }

    private fun addUser(params: UserAdd) : RpcData {
        val pollUser = PollUser(
            googleUserId = params.userId ?: throw IllegalStateException("id param is required"),
            googleAuthToken = params.googleToken,
            name = params.name,
            email = params.email,
            photo = params.photo
        )

        val existingUser = mongo.getCollection<PollUser>().findOne(Filters.eq("googleUserId", pollUser.googleUserId))
        existingUser?.let {
            throw IllegalStateException("There is already a user with this email.")
        }
        mongo.getCollection<PollUser>().insertOne(pollUser)
        return pollUser
    }
}