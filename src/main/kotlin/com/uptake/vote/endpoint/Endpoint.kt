package com.uptake.vote.endpoint

import com.uptake.vote.model.RpcData
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import org.jetbrains.ktor.application.ApplicationRequest
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.http.HttpMethod
import org.jetbrains.ktor.locations.*
import org.jetbrains.ktor.request.httpMethod
import org.jetbrains.ktor.routing.Route

interface Params
interface HasUser {
    fun getUserToken(): String?
}

inline fun <reified R: Params> Route.get(endpoint: Endpoint) {
    get<R> {
        val resp = async(CommonPool) {
            endpoint.handleF(call.request.httpMethod, call.request, it)
        }
        call.respond(resp.await())
    }
}

inline fun <reified R: Params> Route.post(endpoint: Endpoint) {
    post<R> {
        val resp = async(CommonPool) {
            endpoint.handleF(call.request.httpMethod, call.request, it)
        }
        call.respond(resp.await())
    }
}

interface Endpoint {
    suspend fun handleF(method: HttpMethod, request: ApplicationRequest, params: Params) : RpcData
}
