package com.uptake.vote.data.concrete

import com.mongodb.MongoClientURI
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.uptake.vote.data.polls.model.PollUser
import com.uptake.vote.data.services.MongoService
import com.uptake.vote.data.services.getCollection
import org.litote.kmongo.KMongo
import org.litote.kmongo.findOne

class ConcreteMongoService(connectionUrl: String) : MongoService {
    val client = KMongo.createClient(MongoClientURI(connectionUrl))

    override fun getDatabase(): MongoDatabase {
        return client.getDatabase("vote")
    }

    override fun getUser(token: String): PollUser? {
        return getCollection<PollUser>().findOne(Filters.eq("googleAuthToken", token))
    }
}
