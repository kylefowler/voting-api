package com.uptake.vote.data.services

import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.uptake.vote.data.polls.model.PollUser

@Target(AnnotationTarget.CLASS)
annotation class Collection(val name: String)

inline fun <reified T> MongoService.getCollection(name: String? = null): MongoCollection<T> {
    val clazz = T::class
    val annotation = clazz.annotations.singleOrNull { it.annotationClass == Collection::class } as Collection?
    return getDatabase().getCollection(annotation?.name ?: name, T::class.java)
}

interface MongoService {
    fun getDatabase() : MongoDatabase

    fun getUser(token: String): PollUser?
}
