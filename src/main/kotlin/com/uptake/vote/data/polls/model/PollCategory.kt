package com.uptake.vote.data.polls.model

import com.google.gson.annotations.SerializedName
import com.uptake.vote.data.services.Collection
import com.uptake.vote.model.RpcData
import org.bson.types.ObjectId

@Collection("poll_categories")
data class PollCategory(
    @SerializedName("id") val _id: ObjectId? = null,
    val name: String? = null,
    val pollId: ObjectId? = null,
    val options: List<PollOption>? = null
) : RpcData