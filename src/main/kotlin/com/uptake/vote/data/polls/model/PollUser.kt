package com.uptake.vote.data.polls.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uptake.vote.data.services.Collection
import com.uptake.vote.model.RpcData
import org.bson.types.ObjectId

@Collection("poll_users")
data class PollUser(
    @SerializedName("id") val _id: ObjectId? = null,
    val googleUserId : String,
    val email: String? = null,
    @Expose(serialize = false) val googleAuthToken: String? = null,
    val name: String? = null,
    val photo: String? = null
) : RpcData
