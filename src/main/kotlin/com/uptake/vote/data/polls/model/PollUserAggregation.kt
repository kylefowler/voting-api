package com.uptake.vote.data.polls.model

import com.uptake.vote.data.services.Collection
import org.bson.types.ObjectId

/**
 * Collection storing how many votes a user has remaining for a given poll
 */
@Collection("poll_user_aggregation")
data class PollUserAggregation(
    val userId: ObjectId,
    val pollId: ObjectId,
    val votesRemaining: Int = 5,
    val submitted: Boolean = false
)
