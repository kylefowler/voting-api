package com.uptake.vote.data.polls.model

import com.google.gson.annotations.SerializedName
import com.uptake.vote.model.RpcData
import com.uptake.vote.data.services.Collection
import org.bson.types.ObjectId

enum class OptionType {
    Food
}

data class OptionTypeAttributes(
    // Food attributes
    val portionSize: Double? = null,
    val fat: Double? = null,
    val calories: Double? = null,
    val carbs: Double? = null,
    val protein: Double? = null,
    val sodium: Double? = null,
    val sugar: Double? = null,
    val fiber: Double? = null
)

@Collection("poll_options")
data class PollOption(
    @SerializedName("id") val _id : ObjectId? = null,
    val name: String = "",
    val pollId: ObjectId? = null,
    val categoryId: ObjectId? = null,
    val upvoteCount: Int = 0,
    val downvoteCount: Int = 0,
    val description: String? = null,
    val imageUrl: String? = null,
    val type: OptionType? = OptionType.Food,
    val attributes: OptionTypeAttributes? = null,
    val hasUpvoted: Boolean? = false,
    val hasDownvoted: Boolean? = false
) : RpcData
