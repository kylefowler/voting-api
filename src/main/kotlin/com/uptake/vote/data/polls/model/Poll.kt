package com.uptake.vote.data.polls.model

import com.google.gson.annotations.SerializedName
import com.uptake.vote.model.RpcData
import com.uptake.vote.data.services.Collection
import org.bson.types.ObjectId

@Collection("polls")
data class Poll(
    @SerializedName("id") val _id: ObjectId? = null,
    val name: String = "",
    val startTime: Long = 0,
    val endTime: Long = 0,
    val userId: ObjectId? = null,
    val votesPerUser: Int = 5,
    val categories: List<PollCategory>? = null
) : RpcData
