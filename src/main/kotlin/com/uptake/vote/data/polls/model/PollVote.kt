package com.uptake.vote.data.polls.model

import com.google.gson.annotations.SerializedName
import com.uptake.vote.data.services.Collection
import com.uptake.vote.model.RpcData
import org.bson.types.ObjectId

@Collection("poll_votes")
data class PollVote(
    @SerializedName("id") val _id : ObjectId? = null,
    val userId: ObjectId? = null,
    val pollId: ObjectId? = null,
    val pollOptionId: ObjectId? = null,
    val upvote: Boolean = false,
    val downvote: Boolean = false
) : RpcData
