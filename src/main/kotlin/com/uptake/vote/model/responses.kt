package com.uptake.vote.model

import com.uptake.vote.data.polls.model.Poll
import com.uptake.vote.data.polls.model.PollOption

interface RpcData

data class ListResponse<out T>(val items: List<T>) : RpcData

data class PollSummary(
    val votesRemaining: Int = 0
)

data class VoteResponse(
    val voteSummary: PollSummary,
    val option: PollOption
) : RpcData

data class PollDetailResponse(
    val voteSummary: PollSummary,
    val poll: Poll
) : RpcData