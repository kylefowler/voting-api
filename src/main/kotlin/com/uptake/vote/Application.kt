package com.uptake.vote

import org.jetbrains.ktor.application.Application
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.install
import org.jetbrains.ktor.content.TextContent
import org.jetbrains.ktor.features.*
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.locations.*
import org.jetbrains.ktor.logging.CallLogging
import org.jetbrains.ktor.routing.routing
import org.jetbrains.ktor.transform.transform

import com.google.gson.*
import com.uptake.vote.data.concrete.ConcreteMongoService
import com.uptake.vote.endpoint.user.UserEndpoints
import com.uptake.vote.endpoint.poll.PollEndpoints
import com.uptake.vote.endpoint.poll.polls
import com.uptake.vote.endpoint.user.users
import com.uptake.vote.model.RpcData
import org.bson.types.ObjectId
import java.lang.reflect.Type

fun Application.main() {

    install(DefaultHeaders)
    install(CallLogging)
    install(ConditionalHeaders)
    install(PartialContentSupport)
    install(Compression)
    install(Locations)
    install(StatusPages) {
        exception<NotImplementedError> { call.respond(HttpStatusCode.NotImplemented) }
    }

    val gson = GsonBuilder().registerTypeAdapter(ObjectId::class.java, object : JsonSerializer<ObjectId> {
        override fun serialize(src: ObjectId?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
            return JsonPrimitive(src?.toHexString())
        }
    }).create()

    transform.register<RpcData> {
        TextContent(gson.toJson(it), ContentType.Application.Json)
    }

    val inspectarConfig = environment.config.config("vote")
    val databaseConfig = inspectarConfig.config("database")
    val dbString = databaseConfig.property("connection").getString()

    val database = ConcreteMongoService(dbString)

    val pollEndpoint = PollEndpoints(database)
    val userEndpoint = UserEndpoints(database)

    routing {
        polls(pollEndpoint)
        users(userEndpoint)
    }
}
